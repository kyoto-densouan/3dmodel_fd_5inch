# README #

1/3スケールの5インチフロッピーディスク風小物のstlファイルです。 
一部省略しています。 組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 
元ファイルはAUTODESK 123D DESIGNです。 

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_fd_5inch/raw/16e729cba07d376e85d2b2ec29ea68e925af0832/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fd_5inch/raw/16e729cba07d376e85d2b2ec29ea68e925af0832/ModelView_open.png)
